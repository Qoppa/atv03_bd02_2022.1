create table diaria (
codigo_grupo integer not null,
data_inicio_vigencia date not null,
data_fim_vigencia date not null,
valor numeric(10,2) not null,
constraint pk_diaria
    primary key (codigo_grupo,
data_inicio_vigencia),
constraint ck_diaria_vigencia
	check (data_inicio_vigencia <= data_fim_vigencia));

create function	diaria_stamp() returns "trigger" 
as $diaria_stamp$
begin
	if
	new.data_inicio_vigencia > new.data_fim_vigencia then
	
	raise exception 
	'Data inicio não pode ser menor que data fim';
	end if;

if exists
	(
select
	1
from
	diaria d
where
	codigo_grupo = new.codigo_grupo
	and ((data_inicio_vigencia,
	data_fim_vigencia )
	
	overlaps 
	
	(new.data_inicio_vigencia,
	new.data_fim_vigencia))) then 

raise exception 
'Não é possível inserir a diária, pois o período da mesma conflita com o de uma diária já cadastrada na base';

end if;

        RETURN NEW;

end;

$diaria_stamp$
language plpgsql

create trigger diaria_stamp 
before
insert
	on
	diaria
for each row execute function diaria_stamp();


insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(10,'2021-03-01','2021-07-08',50)

insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(10,'2021-05-02','2021-06-01',70)

insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(10,'2021-05-02','2021-11-09',70)

insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(10,'2021-02-01','2021-06-01',70)

insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(10,'2021-02-01','2021-11-09',70)


--susseso

insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(10,'2021-01-01','2021-02-01',70)

insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(10,'2021-10-01','2021-12-01',70)

insert
	into
	diaria (codigo_grupo,
	data_inicio_vigencia,
	data_fim_vigencia,
	valor)
values(20,'2021-05-02','2021-06-01',70)


